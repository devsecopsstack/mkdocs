# GitLab CI template for MkDocs

This project implements a GitLab CI/CD template to build your static website with [MkDocs](https://www.mkdocs.org/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/mkdocs/gitlab-ci-mkdocs@2.5.1
    # 2: set/override component inputs
    inputs:
      site-dir: "build" # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/mkdocs'
    ref: '2.5.1'
    file: '/templates/gitlab-ci-mkdocs.yml'

variables:
  # 2: set/override template variables
  MKD_SITE_DIR: "build" # ⚠ this is only an example
```

## Global configuration

The MkDocs template uses some global configuration used throughout all jobs.

| Input / Variable | Description                            | Default value     |
| ----------------------- | -------------------------------------- | ----------------- |
| `image` / `MKD_IMAGE` | The Docker image used to run MkDocs    | `registry.hub.docker.com/polinux/mkdocs:latest` |
| `workspace-dir` / `MKD_WORKSPACE_DIR` | MkDocs sources directory | `.` |
| `requirements-file` / `MKD_REQUIREMENTS_FILE` | Requirements file. If the file is not found in the repository, requirements are read from the `MKD_REQUIREMENTS` variable | `requirements.txt` |
| `requirements` / `MKD_REQUIREMENTS` | Space separated requirements (ignored if a requirement file is found) | `mkdocs` |
| `site-dir`/ `MKD_SITE_DIR`          | MkDocs generated site directory (relative to `$MKD_WORKSPACE_DIR`), the path will be declared as artifact | `site` |
| `prebuild-script` / `MKD_PREBUILD_SCRIPT` | Pre-build hook script | `mkdocs-pre-build.sh` |
| `PIP_INDEX_URL` | Python repository url | _none_ |
| `pip-opts` / `PIP_OPTS` | pip extra [options](https://pip.pypa.io/en/stable/cli/pip/#general-options) | _none_ |

## Jobs

### `mkdocs` job

This job performs MkDocs **build**. It uses the following variable:

| Input / Variable | Description                                       | Default value     |
| ----------------------- | ------------------------------------------------- | ----------------- |
| `build-args` / `MKD_BUILD_ARGS` | Arguments used by the build job                   | _none_ |

:warning: The built documentation is generated in the `${MKD_WORKSPACE_DIR}/${MKD_SITE_DIR}` folder.
Be sure to update `MKD_SITE_DIR` if you change the output directory (via the `MKD_BUILD_ARGS` variable or the `mkdocs.yml` file).

### `mkdocs-lychee` job

This job checks links with [lychee](https://github.com/lycheeverse/lychee). It uses the following variable:

| Input / Variable | Description                                       | Default value     |
| ----------------------- | ------------------------------------------------- | ----------------- |
| `lychee-enabled` / `MKD_LYCHEE_ENABLED` | Set to `true` to enable this job                  | _none_ (disabled) |
| `lychee-image` / `MKD_LYCHEE_IMAGE` | The Docker image used to run [lychee](https://github.com/lycheeverse/lychee) | `registry.hub.docker.com/lycheeverse/lychee:latest` |
| `lychee-args` / `MKD_LYCHEE_ARGS` | [lychee arguments](https://github.com/lycheeverse/lychee#commandline-parameters) to execute | `docs/` |

## Publishing

:warning: this template is not a deployment template and it only builds a MkDocs project.

You might deploy the generated site using a [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/) job (there is [a variant for that](#gitlab-pages-variant)) or any other method you see fit.

## Variants

### GitLab Pages variant

Basically it copies the content of the mkdocs generated site folder (`site` by default) to the `public` folder which is published by [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/#how-it-works).

If you wish to use it, add the following to your `gitlab-ci.yml`:

```yaml
include:
  # main template
  - component: gitlab.com/to-be-continuous/mkdocs/gitlab-ci-mkdocs@2.5.1
  # GitLab pages variant
  - component: gitlab.com/to-be-continuous/mkdocs/gitlab-ci-mkdocs-pages@2.5.1
```
